import pandas as pd
import matplotlib.pyplot as plt
from datetime import timedelta as td


def data_cleaning(csv_data):
    csv_data = csv_data[csv_data.Quantity > 0]
    list_of_StockCodes = []
    for i in csv_data.StockCode.unique():
        if not i.isdigit():
            list_of_StockCodes.append(i)
    csv_data = csv_data[~csv_data.StockCode.isin(list_of_StockCodes)]
    csv_data.StockCode = csv_data.StockCode.astype(int)
    csv_data['InvoiceDate'] = pd.to_datetime(csv_data['InvoiceDate'], format='%Y-%m-%d %H:%M:%S')
    return csv_data


def add_time_diff(csv_mod_data):
    csv_mod_data = csv_mod_data.sort_values(['StockCode', 'InvoiceDate'])
    csv_mod_data['IntervalTime'] = 0
    csv_mod_data['IntervalClass'] = 0
    csv_mod_data['OtherItems'] = 0
    t0 = td(0)
    r0 = 0

    for i in csv_mod_data.index:
        csv_mod_data.at[i, 'IntervalTime'] = (csv_mod_data.at[i, 'InvoiceDate'] - t0).total_seconds()/60 if csv_mod_data.at[i, 'StockCode'] == r0 else 0
        t0 = csv_mod_data.at[i, 'InvoiceDate']
        r0 = csv_mod_data.at[i, 'StockCode']

    csv_mod_data['OtherItems'] = csv_mod_data.groupby('InvoiceNo')['StockCode'].transform(len)
    print('l')
    csv_mod_data['IntervalClass'] = pd.qcut(csv_mod_data['IntervalTime'], 4, labels=False)
    print(csv_mod_data.head())
    print(csv_mod_data.groupby('InvoiceNo')['StockCode'].count())

    return csv_mod_data


if __name__ == '__main__':

    FILE_NAME = 'retail_data.csv'
    data = pd.read_csv(FILE_NAME)
    print(data.head())
    print(data.info())
    print(data.Quantity.describe())
    print(data.StockCode.describe())
    cleared_data = data_cleaning(data)
    data_ready = add_time_diff(cleared_data)
    print(data_ready.head())
    correlations_data = data_ready.corr()['IntervalTime'].sort_values()
    correlations_data_class = data_ready.corr()['IntervalClass'].sort_values()
    print(correlations_data)
    print(correlations_data_class)
    plt.figure()
    plt.hist2d(data_ready['IntervalTime'], data_ready['UnitPrice'], range=[[0, 150000], [0, 100]])
    plt.show()
    plt.figure()
    plt.scatter(data_ready['IntervalClass'], data_ready['UnitPrice'])
    plt.show()
    print(data_ready.describe())