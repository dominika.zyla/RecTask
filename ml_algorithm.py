import file_check as fc
import pandas as pd
from sklearn import tree
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.neighbors import KNeighborsClassifier


FILE_NAME = 'retail_data.csv'
data = pd.read_csv(FILE_NAME)
data_ready = fc.add_time_diff(fc.data_cleaning(data))

#split data into training and test sets
# decisiontree
train, test = train_test_split(data_ready, test_size=0.5)
model_tree = tree.DecisionTreeClassifier()

model_tree = model_tree.fit(train[['InvoiceNo', 'OtherItems', 'UnitPrice', 'Quantity']], train[['IntervalClass']])
output = model_tree.predict(test[['InvoiceNo', 'OtherItems', 'UnitPrice', 'Quantity']])
acc = accuracy_score(test[['IntervalClass']], output)
print(acc, 'test')
print(confusion_matrix(test[['IntervalClass']], output))
output = model_tree.predict(train[['InvoiceNo', 'OtherItems', 'UnitPrice', 'Quantity']])
acc = accuracy_score(train[['IntervalClass']], output)
print(acc, 'train')
print(confusion_matrix(train[['IntervalClass']], output))

#Knearestneighbours
i = 5
modelknn = KNeighborsClassifier(n_neighbors=i)
modelknn.fit(train[['InvoiceNo', 'OtherItems', 'UnitPrice', 'Quantity']], train[['IntervalClass']])
predicted_labels = modelknn.predict(test[['InvoiceNo', 'OtherItems', 'UnitPrice', 'Quantity']])
predicted_labels1 = modelknn.predict(train[['InvoiceNo', 'OtherItems', 'UnitPrice', 'Quantity']])
accuracy = accuracy_score(test[['IntervalClass']], predicted_labels)
accuracy1 = accuracy_score(train[['IntervalClass']], predicted_labels1)
print(confusion_matrix(test[['IntervalClass']], predicted_labels))
print(confusion_matrix(train[['IntervalClass']], predicted_labels1))
print(accuracy, accuracy1)

